# Hra Snake 
## Popis 
Tento projekt implementuje hru "Snake" v Pythonu pomocí knihovny Pygame. Cílem bylo vytvořit objektově orientovanou aplikaci, která využívá designové vzory pro správu herních mechanik a logiky. Projekt zahrnuje implementaci různých herních funkcí, jako je pohyb hada, růst hada a interakce s jídlem. 
## Hlavní Funkce 
- **State Pattern**: Implementace různých stavů pohybu hada (nahoru, dolů, vlevo, vpravo) jako samostatné třídy. Tento vzor umožňuje čisté oddělení logiky pohybu pro každý směr. 
- **Factory Pattern**: Třída `FoodFactory` generuje nové instance jídla a spravuje jejich rozmístění na herní ploše. Tento vzor umožňuje flexibilní a rozšiřitelný přístup k vytváření a správě jídla ve hře.
- **Singleton Pattern**: Implementace třídy `Snake` jako singletonu zajišťuje, že v rámci hry existuje pouze jedna instance hada. Tento vzor je vhodný pro udržení konzistence a zamezení konfliktům, pokud by bylo třeba více instancí hada.
- **Dokumentace**: Všechny třídy a metody obsahují podrobné docstringy, které popisují účel a použití jednotlivých komponentů a usnadňují pochopení a údržbu kódu.
 ## Jak Spustit Projekt 
 1. **Instalace závislostí**: ```pip install pygame ``` 
 2. **Spuštění hry**: ```python main.py ```
 3. **Spuštění testů**: ```python -m unittest discover -s tests ```
 ## Testování 
 Projekt obsahuje jednotkové testy pro ověření základních herních mechanik: - **Testy pohybu hada**: Ověřují správnost pohybu hada v různých směrech. - **Testy růstu hada**: Ověřují, že had roste po konzumaci jídla. - **Testy interakce s jídlem**: Ověřují generování a odstraňování jídla. Testy jsou umístěny ve složce `tests`. Testy můžete spustit pomocí následujícího příkazu:  ```python -m unittest discover -s tests ```
 ## Dokumentace 
 Každá třída a metoda v kódu obsahuje docstringy podle standardu PEP 257. Tyto docstringy poskytují informace o účelu a použití jednotlivých komponentů. 
  ## Designové Vzory
- **State Pattern**: Použití pro řízení pohybu hada v různých směrech.
- **Factory Pattern**: Použití pro generování a správu jídla ve hře.
- **Singleton Pattern**: Zajišťuje, že ve hře existuje pouze jedna instance hada, čímž se zamezuje možným konfliktům a zajišťuje konzistence herní logiky.
 ## Odkazy
**Dokumentace k designovým vzorům:**

 - [Design Patterns](https://en.wikipedia.org/wiki/Design_Patterns) 
 -  [State Patterns](https://refactoring.guru/design-patterns/state) 
 - [Singleton Pattern](https://refactoring.guru/design-patterns/singleton) 
 - [Factory Method](https://refactoring.guru/design-patterns/factory-method)
 
 **Zdroje inspirace:**
  - [Snake Game in Python - Full Tutorial](https://www.youtube.com/watch?v=bfRwxS5d0SI&t=60s) 
  - [Pygame Tutorial for Beginners](https://www.youtube.com/watch?v=jO6qQDNa2UY) 
  - [Build a Snake Game in Python!](https://www.youtube.com/watch?v=AY9MnQ4x3zk&t=306s) 
  - Mé předchozí projekty

 ## Závěr
Tento projekt byl vytvořen s cílem oprášit a uplatnit znalosti získané během středoškolského studia, zejména návrhových vzorů, které jsem dříve používal v jazycích jako C# a C++. Bylo zajímavé tyto vzory aplikovat v Pythonu a sledovat, jak lze stejná koncepční řešení přenést do jiného programovacího jazyka. Projekt také ukazuje, jak mohou být další návrhové vzory, jako například Observer, využity v budoucích verzích hry pro ještě složitější herní mechaniky.
