from __future__ import annotations
from copy import deepcopy
import csv


class Matrix:
    def __init__(self, matrix: list[float] | str = None, num_of_rows: int = None):
        self.matrix = []

        # Rozhodne, zda se bude načítat matice z příkazového řádku od uživatele, zda už uživatel při vytváření dodal seznam řádků případně zda se má načíst ze souboru
        if matrix and isinstance(matrix, list):
            self._code_init(matrix)
        elif matrix and isinstance(matrix, str):
            self._file_init(matrix)
        else:
            self._terminal_init(num_of_rows)

        # Inicializuje pomocné parametry matice
        self.num_of_rows = len(self.matrix)
        self.num_of_columns = len(self.matrix[0])

    # Načte z dodaného listu řádků matici
    # Zastaví program v případě chybných hodnot
    def _code_init(self, matrix: list[float]) -> None:
        for row in matrix:
            # Ověří, že všechny dodané hodnoty jsou čísla
            try:
                row = list(map(float, row))
            except ValueError:
                raise ValueError("!!! Některá z hodnot nebyla celé číslo. Prosím zadávejte jen celá čísla. !!!")

            # Ověří, že buď jde o první řádek, nebo je zadáván řádek o stejné délce, jako měl ten první
            if len(self.matrix) == 0 or len(row) == len(self.matrix[0]):
                self.matrix.append(row)
                continue

            raise ValueError("!!! Některý z řádků nemá stejnou délku, jako měl ten první. Prosím zadávejte řádky o stejné délce. !!!")

    # Načte matici ze vstupu od uživatele
    def _terminal_init(self, num_of_rows: int = None) -> None:
        # Pokud nebyl zadán počet řádků, vyžádá si počet od uživatele
        if num_of_rows is None:
            while True:
                try:
                    num_of_rows = int(input("Zadejte, kolik řádku by matice měla mít: "))
                    break
                except ValueError:
                    print("!!! Zadaná hodnota není přirozené číslo. Prosím zadejte přirozené číslo. !!!")
                    continue

        self.matrix.append(self._request_row(first=True))  # Načte první řádek

        # Načte všechny ostatní řádky
        for _ in range(num_of_rows - 1):
            self.matrix.append(self._request_row())

    # Získá od uživatele z příkazového řádku jeden řádek matice
    # U toho ověřuje, zda uživatel zadává jen celá čísla
    # Pokud se nejedná o první řádek, rovněž ověřuje, zda každý další řádek má stejný počet prvků jako měl ten první
    def _request_row(self, first: bool = False) -> list:
        while True:
            # Ověří, že všechny dodané hodnoty jsou čísla
            try:
                row = list(map(float, filter(None, input("Zadejte řádek matice a oddělujte jednotlivé hodnoty jednou mezerou: ").split())))
            except ValueError:
                print("!!! Některá z hodnot nebylo celé číslo. Prosím zadávejte jen celá čísla. !!!")
                continue

            # Ověří, že buď jde o první řádek, nebo je zadáván řádek o stejné délce, jako měl ten první
            if first or len(row) == len(self.matrix[0]):
                return row

            print("!!! Řádek nemá stejnou délku, jako měl ten první. Prosím zadávejte řádky o stejné délce. !!!")

    # Načte matice z CSV souboru
    # U toho ověřuje veškeré náležitosti (že jsou zadávána jen čísla, koreknost délky řádků, ...)
    def _file_init(self, file_path: str) -> None:
        try:
            with open(file_path) as matrix_file:
                csv_reader = csv.reader(matrix_file, delimiter=',')

                future_matrix = []

                # Postupně přidává řádky do matice dle csv souboru
                for csv_row in csv_reader:
                    matrix_row = []
                    for csv_column in csv_row:
                        try:
                            matrix_row.append(float(csv_column))
                        except ValueError:
                            raise ValueError("!!! Některá z hodnot nebyla celé číslo. Prosím zadávejte jen celá čísla. !!!")
                    future_matrix.append(matrix_row)
        except FileNotFoundError:
            raise FileNotFoundError("!!! Soubor, který chcete otevřít, neexistuje. !!!")

        self._code_init(future_matrix)

    # Pomocná funkce, která slouží k navrácení původní matice po provedených změnách, pokud si to uživatel přeje
    def _reset_matrix(self, inplace: bool, temp: Matrix) -> None:
        if not inplace:
            self.matrix = temp

    # Transponuje matici
    def transpose(self, inplace: bool = False) -> Matrix:
        transposed_matrix = []

        # Postupně vytvoří ze sloupců původní matice řádky nové matice
        for column_index in range(self.num_of_columns):
            new_row = []

            for row in self.matrix:
                new_row.append(row[column_index])

            transposed_matrix.append(new_row)

        if inplace:
            self.__init__(transposed_matrix)

        return Matrix(transposed_matrix)

    # Vynásobí řádek nenulovým číslem, případně upozorní uživatele, že chce násobit nulou
    # Používá indexaci od 1 do n (včetně)
    # Operaci provede rovnou i na jednotkové matici
    def multiply_row_by_num(self, row_index: int, scalar: float = 1) -> int:
        if scalar == 0:
            print("!!! Řádek se dá násobit pouze nenulovým číslem. Prosím použijte nenulové číslo a zkuste použít znovu. !!!")
            return 1

        self.matrix[row_index - 1] = [value * scalar for value in self.matrix[row_index - 1]]
        return scalar

    # Prohodí dva řádky
    # Používá indexaci od 1 do n (včetně)
    # Operaci provede rovnou i na jednotkové matici
    def swap_rows(self, first_row_index: int, second_row_index: int) -> int:
        self.matrix[first_row_index - 1], self.matrix[second_row_index - 1] = self.matrix[second_row_index - 1], self.matrix[first_row_index - 1]
        return -1

    # Přičte násobek řádku s indexem target_row_index k řádku s indexem source_row_index
    # Používá indexaci od 1 do n (včetně)
    # Operaci provede rovnou i na jednotkové matici
    def add_rows(self, target_row_index: int, source_row_index: int, scalar: float = 1) -> None:
        self.matrix[target_row_index - 1] = [value1 + scalar * value2 for value1, value2 in zip(self.matrix[target_row_index - 1], self.matrix[source_row_index - 1])]

    # Provede Gaussovu eliminaci a převede matici do horního trojúhelníkového tvaru
    def upper_triangularize(self, inplace: bool | None = False) -> int:
        temp = deepcopy(self.matrix)
        determinant_mod = 1

        # Postupně projde všechny sloupce, a vynuluje čísla pod hlavní diagonálou
        for column_index in range(self.num_of_columns):
            for row_index in range(column_index + 1, self.num_of_rows):

                # Pokud je na hlavní diagonále nula, projde celý zbytek sloupce a pokud to půjde (bude patřičný řáde existovat), prohodí řádky tak, aby na hlavní diagonále bylo nenulové číslo
                if self.matrix[column_index][column_index] == 0:
                    for swap_row_index in range(row_index, self.num_of_rows):
                        if self.matrix[swap_row_index][column_index] == 0:
                            continue
                        determinant_mod *= self.swap_rows(swap_row_index + 1, row_index)

                # Určí, jakou konstantou by se měl vynásobit přičítaný řádek
                try:
                    scalar = -self.matrix[row_index][column_index] / self.matrix[column_index][column_index]
                except ZeroDivisionError:
                    continue

                # Pokud by se měl přičítaný řádek násobit nulou (tzn. na zkoumané pozici pod hlavní diagonálou už je nula), není nutné tuto operaci provádět
                if scalar == 0:
                    continue

                self.add_rows(row_index + 1, column_index + 1, scalar)

        self._reset_matrix(inplace, temp)

        return determinant_mod

    # Provede Gaussovu eliminaci a převede matici do dolního trojúhelníkového tvaru
    def lower_triangularize(self, inplace: bool = False) -> int:
        temp = deepcopy(self.matrix)
        determinant_mod = 1

        if self.num_of_rows != self.num_of_columns:
            difference = self.num_of_columns - self.num_of_rows

        else:
            difference = 0

        # Postupně projde všechny sloupce, a vynuluje čísla nad hlavní diagonálou
        for column_index in reversed(range(difference, self.num_of_columns)):
            for row_index in reversed(range(column_index - difference)):

                # Pokud je na hlavní diagonále nula, projde celý zbytek sloupce a pokud to půjde (bude patřičný řáde existovat), prohodí řádky tak, aby na hlavní diagonále bylo nenulové číslo
                if self.matrix[column_index - difference][column_index] == 0:
                    for swap_row_index in reversed(range(row_index + 1)):
                        if self.matrix[swap_row_index][column_index] == 0:
                            continue
                        determinant_mod *= self.swap_rows(swap_row_index + 1, row_index + 2)

                # Určí jakým koeficientem by se měl vynásobit přičítaný řádek
                try:
                    scalar = -self.matrix[row_index][column_index] / self.matrix[column_index - difference][column_index]
                except ZeroDivisionError:
                    continue

                # Pokud by se měl přičítaný řádek násobit nulou (tzn. na zkoumané pozici pod hlavní diagonálou už je nula), není nutné tuto operaci provádět
                if scalar == 0:
                    continue

                self.add_rows(row_index + 1, column_index - difference + 1, scalar)

        self._reset_matrix(inplace, temp)

        return determinant_mod

    def get_rank(self) -> int:
        temp = deepcopy(self.matrix)
        temp_matrix = Matrix(temp)
        temp_matrix.upper_triangularize(inplace=True)

        self._reset_matrix(inplace=False, temp=temp)

        return sum(any(element != 0 for element in row) for row in temp_matrix.matrix)

    def get_linear_independance(self) -> bool:
        return self.get_rank() == self.num_of_columns

    # Vynásobí dvě zadané matice
    @staticmethod
    def multiply(matrix_a: Matrix, matrix_b: Matrix) -> Matrix:
        if len(matrix_a.matrix[0]) != len(matrix_b.matrix):
            raise ValueError("Není možné tyto dvě matice vynásobit.")

        result = []

        # Postupně prochází řádky matice A a slopce matice B a vytváří řádky výsledné matice C, kterou vrátí
        for matrix_a_row_index in range(matrix_a.num_of_rows):
            row = []
            for matrix_b_column_index in range(matrix_b.num_of_columns):
                row.append(sum(matrix_a.matrix[matrix_a_row_index][iterator] * matrix_b.matrix[iterator][matrix_b_column_index] for iterator in range(matrix_b.num_of_rows)))
            result.append(row)

        return Matrix(result)

    # Vrátí vhodný výstup pokud uživatel zavolá print(matice)
    def __str__(self) -> str:
        return Matrix._print_matrix(self.matrix, self.num_of_rows, self.num_of_columns)

    # Upraví výstup matice tak, aby byly zarovnány sloupce správně pod sebou dle desetinných míst
    @staticmethod
    def _print_matrix(matrix: list[float], num_of_rows: int = 0, num_of_columns: int = None) -> str:
        if num_of_rows == 0:
            return "Prázdná matice"

        if num_of_columns is None:
            num_of_columns = num_of_rows

        # Najde nejširší prvek v každém sloupci a nejvyšší počet desetinných míst
        column_widths = [max(len(str(matrix[row_index][column_index]).split(".")[0]) for row_index in range(num_of_rows)) for column_index in range(num_of_columns)]
        decimal_places = [max(len(str(matrix[row_index][column_index]).split(".")[1]) if "." in str(
            matrix[row_index][column_index]) else 0 for row_index in range(num_of_rows)) for column_index in range(num_of_columns)]

        # Po řádcích vytvoří output na základě potřeb pro šířku sloupců; zarovnává doprava
        output = ""
        for row_index in range(num_of_rows):
            row_output = " | ".join(
                f"{value:>{column_widths[column_index] + decimal_places[column_index] + 1}.{decimal_places[column_index]}f}" if "." in str(float(value)) else f"{value:<{column_widths[column_index]}}"
                for column_index, value in enumerate(matrix[row_index])
            )
            output += f"{row_output}\n"

        return output


class SquareMatrix(Matrix):
    def __init__(self, matrix: list[float] = None):
        super().__init__(matrix, num_of_rows=None)

        # Přidané doplňkové parametry čtvercové matice, které u obdélníkové nemají smysl
        self.order = self.num_of_rows
        self.unit_matrix = [[1.0 if i == j else 0 for j in range(self.order)] for i in range(self.order)]

    # Načte z dodaného listu řádků matici
    # Zastaví program v případě chybných hodnot
    # Před odvoláním se na funkci z objektu Matrix zkontroluje čtvercovost
    def _code_init(self, matrix: list[float]) -> None:
        if len(matrix) != len(matrix[0]):
            raise ValueError("!!! Matice na vstupu není čtvercová. Pro použití třídy SquareMatrix zadejte čtvercovou matici. !!!")

        return super()._code_init(matrix)

    # Znovu vygeneruje jednotkovou matici
    def reset_unit(self) -> None:
        self.unit_matrix = [[1.0 if i == j else 0 for j in range(self.order)] for i in range(self.order)]

    # Pomocná funkce, která slouží k navrácení původní matice po provedených změnách, pokud si to uživatel přeje
    def _reset_matrix(self, inplace: bool, temp: SquareMatrix) -> None:
        super()._reset_matrix(inplace, temp)
        if not inplace:
            self.reset_unit()

    # Vynásobí řádek nenulovým číslem, případně upozorní uživatele, že chce násobit nulou
    # Používá indexaci od 1 do n (včetně)
    # Operaci provede rovnou i na jednotkové matici
    def multiply_row_by_num(self, row_index: int, scalar: float = 1) -> int:
        if scalar == 0:
            print("!!! Řádek se dá násobit pouze nenulovým číslem. Prosím použijte nenulové číslo a zkuste použít znovu. !!!")
            return 1

        super().multiply_row_by_num(row_index, scalar)

        self.unit_matrix[row_index - 1] = [value * scalar for value in self.unit_matrix[row_index - 1]]
        return scalar

    # Prohodí dva řádky
    # Používá indexaci od 1 do n (včetně)
    # Operaci provede rovnou i na jednotkové matici
    def swap_rows(self, first_row_index: int, second_row_index: int) -> int:
        super().swap_rows(first_row_index, second_row_index)
        self.unit_matrix[first_row_index - 1], self.unit_matrix[second_row_index - 1] = self.unit_matrix[second_row_index - 1], self.unit_matrix[first_row_index - 1]
        return -1

    # Přičte násobek řádku s indexem target_row_index k řádku s indexem source_row_index
    # Používá indexaci od 1 do n (včetně)
    # Operaci provede rovnou i na jednotkové matici
    def add_rows(self, target_row_index: int, source_row_index: int, scalar: float = 1) -> None:
        super().add_rows(target_row_index, source_row_index, scalar)
        self.unit_matrix[target_row_index - 1] = [value1 + scalar * value2 for value1, value2 in zip(self.unit_matrix[target_row_index - 1], self.unit_matrix[source_row_index - 1])]

    # Vypočítá determinant zadané matice pomocí převedení na horní trojúhelníkový tvar a následné snásobení prvků na hlavní diagonále
    def get_determinant(self, inplace: bool = False) -> int:
        determinant_mod = 1
        temp = deepcopy(self.matrix)

        determinant_mod *= self.upper_triangularize(inplace=True)

        determinant = 1
        for index in range(self.order):
            determinant *= self.matrix[index][index]

        self._reset_matrix(inplace, temp)

        return determinant_mod * determinant

    # Vratí True pokud je matice regulární (má nenulový determinant), False pokud je singulární (má nulový determinant)
    def is_invertable(self, inplace: bool | None = False) -> bool:
        return self.get_determinant(inplace=inplace) != 0

    # Provede úplnou Gaussovu eliminaci v několika krocích:
    # 1. určí zda je matice regulární (tím se zároveň dostane do HTT)
    # 2. převede matici v HTT do DTT (tím získáme nenulové prvky jen na hlavní diagonále)
    # 3. převede prvky na hlavní diagonále na 1
    # Protože se všechny úpravy simultáně dějou i na jednotkové matici, vrátí úpravenou jednotkovu matici, která je nyní inverzní k původní zadané matici
    def full_gaussian(self, inplace: bool = False) -> SquareMatrix:
        temp = deepcopy(self.matrix)

        self.reset_unit()

        # Ověří regularitu matici (zároveň ji převede do HTT) a případně zastaví běh programu
        if not self.is_invertable(inplace=True):
            print("!!! Zadaná matice je singulární. Prosím zadejte regulární matici a zkuste to znovu. !!!")
            self._reset_matrix(inplace, temp)
            return

        # Převede matici do DTT
        self.lower_triangularize(inplace=True)

        # Dostane na hlavní diagonálu jen jedničky
        for row_index in range(self.order):
            self.multiply_row_by_num(row_index + 1, 1 / self.matrix[row_index][row_index])

        # Vrátí inverzní matici
        inverse_matrix = self.unit_matrix
        self._reset_matrix(inplace, temp)
        return SquareMatrix(inverse_matrix)

    # Vrátí k vytisknutí jednotkovu matici (slouží např. k testování)
    def print_unit(self) -> str:
        return Matrix._print_matrix(self.unit_matrix, self.order)
