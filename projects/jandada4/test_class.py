from mop.matrix import Matrix, SquareMatrix


class TestClass():
    def test_transposition(self):
        matrix = Matrix([[1, 2, 3],
                         [4, 5, 6]])

        assert matrix.transpose().matrix == [[1, 4],
                                             [2, 5],
                                             [3, 6]]

    def test_row_multiplication(self):
        matrix = Matrix([[1, 2, 3],
                         [4, 5, 6]])

        assert matrix.multiply_row_by_num(1, 2) == 2  # Testing that the correct value (determinant mod) is returned
        assert matrix.matrix == [[2, 4, 6],
                                 [4, 5, 6]]  # Testing that the correct values got correctly multiplied

    def test_row_addition(self):
        matrix = Matrix([[1, 2, 3],
                         [4, 5, 6]])

        assert matrix.add_rows(2, 1, -2) == None  # Testing that the correct value (determinant mod) is returned
        assert matrix.matrix == [[1, 2, 3],
                                 [2, 1, 0]]  # Testing that the correct values got correctly multiplied

    def test_row_swap(self):
        matrix = Matrix([[1, 2, 3],
                         [4, 5, 6]])

        assert matrix.swap_rows(1, 2) == -1  # Testing that the correct value (determinant mod) is returned
        assert matrix.matrix == [[4, 5, 6],
                                 [1, 2, 3]]  # Testing that the correct values got correctly multiplied

    def test_row_ops_on_square_matrix(self):
        matrix = SquareMatrix([[1, 2, 3],
                               [4, 5, 6],
                               [7, 8, 9]])

        matrix.add_rows(1, 2, 2)
        matrix.multiply_row_by_num(2, 2)
        matrix.swap_rows(1, 3)

        # The individual matrix operations were tested one by one on a general matrix, this test now serves to check if the operations are being completed on a unit matrix if working with a square matrix
        assert matrix.unit_matrix == [[0, 0, 1],
                                      [0, 2, 0],
                                      [1, 2, 0]]

    def test_upper_triangular_form(self):
        matrix = SquareMatrix([[1, 2, 3],
                              [4, 5, 6],
                              [7, 8, 9]])

        matrix.upper_triangularize()  # Tests the proper functioning of the inplace keyword. With out it, the matrix should be modified.
        assert matrix.matrix == [[1, 2, 3],
                                 [4, 5, 6],
                                 [7, 8, 9]]

        matrix.upper_triangularize(inplace=True)  # Tests the upper triangularize function
        assert matrix.matrix == [[1.0, 2.0, 3.0],
                                 [0.0, -3.0, -6.0],
                                 [0.0, 0.0, 0.0]]

    def test_multiplication(self):
        matrix_a = SquareMatrix([[1, 2, 3],
                                 [4, 5, 6],
                                 [7, 8, 9]])

        matrix_b = Matrix([[1, 4],
                           [2, 5],
                           [3, 6]])

        assert Matrix.multiply(matrix_a, matrix_b).matrix == [[14.0, 32.0],
                                                              [32.0, 77.0],
                                                              [50.0, 122.0]]

    def test_rank(self):
        matrix = SquareMatrix([[1, 2, 3],
                               [4, 5, 6],
                               [7, 8, 9]])

        assert matrix.get_rank() == 2

    def test_linear_independance(self):
        matrix = SquareMatrix([[1, 2, 3],
                               [4, 5, 6],
                               [7, 8, 9]])

        assert not matrix.get_linear_independance()

    def test_determinant(self):
        matrix = SquareMatrix([[5, 3, 7],
                               [2, 4, 9],
                               [3, 6, 4]])

        assert matrix.get_determinant() == -133

    def test_invertability(self):
        matrix_a = SquareMatrix([[1, 2, 3],
                                 [4, 5, 6],
                                 [7, 8, 9]])

        matrix_b = SquareMatrix([[5, 3, 7],
                                 [2, 4, 9],
                                 [3, 6, 4]])

        assert not matrix_a.is_invertable()
        assert matrix_b.is_invertable()

    def test_inverse(self):
        matrix = SquareMatrix([[3, -2, 4],
                               [1, 0, 2],
                               [0, 1, 0]])

        assert matrix.full_gaussian().matrix == [[1, -2, 2],
                                                 [8.326672684688674e-17, 0, 1],
                                                 [-0.5, 1.5, -1]]  # Value 8.326672684688674e-17 cause by floating point computations. In fact the value should be zero.
