import sys, time
import usersSnapshots as us

if __name__ == "__main__":
    for _ in range(int(sys.argv[1])):
        us.createSnapshot()
        time.sleep(0.5)
        print("New snapshot manually created, time: {}".format(us.getCurrentTime()))
